<?php
require_once __DIR__. '/include/header.php';

?>

      <!-- services section start -->
      <div class="services_section layout_padding">
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <h1 class="services_taital">Services</h1>
                  <p class="services_text">nous proposons les services suivants </p>
               </div>
            </div>
            <div class="services_section_2">
               <div class="row">
                  <div class="col-lg-4 col-sm-12 col-md-4">
                     <div class="box_main active">
                        <div style="height: 80px;" class="house_icon">
                           
                           <img src="images/icon1.png" class="image_2">
                        </div>
                        <h3 class="decorate_text">ventes d'appareil electronique</h3>
                        <p class="tation_text">nous mettons a votre dispositions des ordinateurs ,cameras de tres bonne capacité des cameras ...  </p>
                        <div class="readmore_bt"><a href="..\monoshop\index.php">Decouvrir d'avantage</a></div>
                     </div>
                  </div>
                  <div class="col-lg-4 col-sm-12 col-md-4">
                     <div class="box_main">
                        <div style="height: 110px;"class="house_icon">
                           
                           <img src="images/icon2.png" class="image_2">
                        </div>
                        <h3 class="decorate_text">Service de maintenance</h3>
                        <p class="tation_text">Pour tout appareil acheté beneficier d'une maintenance gratuite d'un mois pour cet appareil </p>
                        <div class="readmore_bt"><a href="#">Read More</a></div>
                     </div>
                  </div>
                  <div class="col-lg-4 col-sm-12 col-md-4">
                     <div class="box_main">
                        <div style="height: 30px;"
                        class="house_icon">
                           
                           <img src="images/icon3.png" class="image_2">
                        </div>
                        <h3 class="decorate_text">Formation en informatique</h3>
                        <p class="tation_text">nous proposons egualent des seances de formation en informatique dans les domaines tel que l'infographie, le genie logiciel, la maintenance , reseau...</p>
                        <div class="readmore_bt"><a href="#">Read More</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- services section end -->
      <?php
require_once __DIR__. '/include/footer.php';

?>
